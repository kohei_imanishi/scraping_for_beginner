.. kikagaku documentation master file, created by
   sphinx-quickstart on Tue Apr 10 18:53:02 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Webスクレイピング入門にようこそ！
====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :glob:

   src/0
   src/1
   src/2
   src/3
   src/4
   src/5

==================
